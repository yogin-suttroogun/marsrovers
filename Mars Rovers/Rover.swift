//
//  Rover.swift
//  Mars Rovers
//
//  Created by Suttroogun Yogin on 02/11/2021.
//

import UIKit

// Outlining question details:
// Rover: Position and location => (x, y, Z) where z is the facing position in {N, E, W, S}
// Plateau: Grid of positions => (x,y, Z), where (0, 0, N) => (x=0, y=0, Z=N)
//          MaximumCoordinates => (maxX, maxY) => Assumption
// Instruction message: e.g AAAAAAA => where a in L,R,M => L= SpinLeft & R= SpinRight by by 180 degrees respectively
//                                                         M= MoveForward
// Expected behaviours: Rover
//                      1. Spin left
//                      2. Spin Right
//                      3. Move Forward
// Limitation: Cannot move to a position where (X+Y) is a prime number

class Rover: UIViewController {
    
    //    MARK: - Variables
    let roverDataManager = RoverDataManager(location: "")
    let placeholderLocationValue = "1 2 N"
    let placeholderInstructionValue = "LMLMLMLMM"
    
//    MARK: - UI Components
    @IBOutlet weak var firstRoverStartLocationTxtFld: UITextField! {
        didSet {
            firstRoverStartLocationTxtFld.placeholder = placeholderLocationValue
        }
    }
    @IBOutlet weak var firstRoverInstructionTxtFld: UITextField! {
        didSet {
            firstRoverInstructionTxtFld.placeholder = placeholderInstructionValue
        }
    }
    @IBOutlet weak var firstRoverCalculateLocationBtn: UIButton!
    
    @IBOutlet weak var secondRoverStartLocationTxtFld: UITextField! {
        didSet {
            secondRoverStartLocationTxtFld.placeholder = placeholderLocationValue
            secondRoverStartLocationTxtFld.isEnabled = false
        }
    }
    @IBOutlet weak var secondRoverInstructionTxtFld: UITextField! {
        didSet {
            secondRoverInstructionTxtFld.placeholder = placeholderInstructionValue
            secondRoverInstructionTxtFld.isEnabled = false
        }
    }
    @IBOutlet weak var secondRoverCalculateLocationBtn: UIButton! {
        didSet {
            secondRoverCalculateLocationBtn.isEnabled = false
        }
    }
    
    @IBOutlet weak var firstRoverResultLbl: UILabel!
    @IBOutlet weak var secondRoverResultLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDataManager()
    }
    
    func initDataManager() {
        self.roverDataManager.showAlertClosure = { [weak self] () in
            self?.showAlert(title: "Something went wrong", message: "Invalid values entered!")
        }
    }

//    MARK: - UI Button Action
    @IBAction func firstRoverCalculateLocationBtnTap(_ sender: Any) {
        if firstRoverStartLocationTxtFld.text?.isEmpty == false && firstRoverInstructionTxtFld.text?.isEmpty == false {
            self.roverDataManager.segregateLocation(firstRoverStartLocationTxtFld.text ?? "")
            self.roverDataManager.travelToLocation(command: firstRoverInstructionTxtFld.text ?? "")
            self.firstRoverResultLbl.text = "\(roverDataManager.x) \(roverDataManager.y) \(roverDataManager.direction)"
            
            enableSecondRover()
        } else {
            self.showAlert(title: "Something went wrong", message: "Please enter correct values!")
        }
        
    }
    
    @IBAction func secondRoverCalculateLocationBtnTap(_ sender: Any) {
        if secondRoverStartLocationTxtFld.text?.isEmpty == false && secondRoverInstructionTxtFld.text?.isEmpty == false {
            self.roverDataManager.segregateLocation(secondRoverStartLocationTxtFld.text ?? "")
            self.roverDataManager.travelToLocation(command: secondRoverInstructionTxtFld.text ?? "")
            self.secondRoverResultLbl.text = "\(roverDataManager.x) \(roverDataManager.y) \(roverDataManager.direction)"
        } else {
            self.showAlert(title: "Something went wrong", message: "Please enter correct values!")
        }
    }
    
//    MARK: - Other methods
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func enableSecondRover() {
        self.secondRoverStartLocationTxtFld.isEnabled = true
        self.secondRoverInstructionTxtFld.isEnabled = true
        self.secondRoverCalculateLocationBtn.isEnabled = true
    }
}

