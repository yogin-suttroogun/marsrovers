//
//  RoverDataManager.swift
//  Mars Rovers
//
//  Created by Suttroogun Yogin on 03/11/2021.
//

import Foundation

class RoverDataManager {
    //    MARK: - Variables
    var x: Int = 0
    var y: Int = 0
    var direction: String = ""
    
    enum CardinalDirections: String {
        case north = "N"
        case east = "E"
        case south = "S"
        case west = "W"
    }
    
    enum Instructions: Character {
        case left = "L"
        case right = "R"
        case forward = "M"
    }
    
    //MARK:- Closures
    var showAlertClosure: (()->())?
    
    //    MARK: - Init method
    init(location: String) {
        segregateLocation(location)
    }
    
    public func segregateLocation(_ location: String) {
        if !location.isEmpty && Array(location).count > 3 {
            self.x = Int(location.split(separator: " ")[0]) ?? 0
            self.y = Int(location.split(separator: " ")[1]) ?? 0
            direction = String(location.split(separator: " ")[2])
        } else {
            self.showAlertClosure?()
        }
    }
    
    func splitLocationDetails(_ location: String) {
        self.x = Int(location.split(separator: " ")[0]) ?? 0
        self.y = Int(location.split(separator: " ")[1]) ?? 0
        self.direction = String(location.split(separator: " ")[2])
    }
    
    //    MARK: - Instructions
    ///Spin left by 180 degrees
    func spinLeft() {
        switch direction {
        case CardinalDirections.north.rawValue:
            direction = CardinalDirections.south.rawValue
        case CardinalDirections.east.rawValue:
            direction = CardinalDirections.west.rawValue
        case CardinalDirections.south.rawValue:
            direction = CardinalDirections.north.rawValue
        case CardinalDirections.west.rawValue:
            direction = CardinalDirections.east.rawValue
        default:
            self.showAlertClosure?()
            break
        }
    }
    
    ///Spin right by 180 degrees
    func spinRight() {
        switch direction {
        case CardinalDirections.north.rawValue:
            direction = CardinalDirections.south.rawValue
        case CardinalDirections.east.rawValue:
            direction = CardinalDirections.west.rawValue
        case CardinalDirections.south.rawValue:
            direction = CardinalDirections.north.rawValue
        case CardinalDirections.west.rawValue:
            direction = CardinalDirections.east.rawValue
        default:
            self.showAlertClosure?()
            break
        }
    }
    
    ///Step forward
    func stepForward() {
        var moveToBeMade: Int = 0
        
        switch direction {
        case CardinalDirections.north.rawValue:
            moveToBeMade = y + 1
            if checkIfSumOfCoordinateIsPrime(x: x, y: moveToBeMade) == false {
                y = moveToBeMade
            }
        case CardinalDirections.east.rawValue:
            moveToBeMade = x + 1
            if checkIfSumOfCoordinateIsPrime(x: moveToBeMade, y: y) == false {
                x = moveToBeMade
            }
        case CardinalDirections.south.rawValue:
            moveToBeMade = y - 1
            if checkIfSumOfCoordinateIsPrime(x: x, y: moveToBeMade) == false {
                y = moveToBeMade
            }
        case CardinalDirections.west.rawValue:
            moveToBeMade = x - 1
            if checkIfSumOfCoordinateIsPrime(x: moveToBeMade, y: y) == false {
                x = moveToBeMade
            }
        default:
            self.showAlertClosure?()
            break
        }
    }
    
    /// Travel to location based on set of instructions
    func travelToLocation(command: String) {
        let instructions = Array(command)
        instructions.forEach { (character) in
            switch character {
            case Instructions.left.rawValue:
                spinLeft()
            case Instructions.right.rawValue:
                spinRight()
            case Instructions.forward.rawValue:
                stepForward()
            default:
                self.showAlertClosure?()
                break
            }
        }
    }
    
    ///Check if the sum of coordinate x and y is prime
    func checkIfSumOfCoordinateIsPrime(x: Int, y: Int) -> Bool {
        let sum = x + y
        return isPrime(sum)
    }
    
    /// Check if prime
    func isPrime(_ n: Int) -> Bool {
        guard n >= 2     else { return false }
        guard n != 2     else { return true  }
        guard n % 2 != 0 else { return false }
        return !stride(from: 3, through: Int(sqrt(Double(n))), by: 2).contains { n % $0 == 0 }
    }
    
}
