//
//  Mars_RoversTests.swift
//  Mars RoversTests
//
//  Created by Suttroogun Yogin on 02/11/2021.
//

import XCTest
@testable import Mars_Rovers

class Mars_RoversTests: XCTestCase {
    var roverDataManager: RoverDataManager!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        roverDataManager = RoverDataManager(location: "")
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        roverDataManager = nil
    }
    
    func testPositive_spinLeft() {
        //Arrange //Data
        roverDataManager = RoverDataManager(location: "1 2 N")
        
        //Act //Calls
        roverDataManager.spinLeft()
        
        //Assert //Checks
        XCTAssertEqual("S", roverDataManager.direction)
    }
    
    func testNegative_spinLeft() {
        //Arrange //Data
        roverDataManager = RoverDataManager(location: "1 2 N")
        
        //Act //Calls
        roverDataManager.spinLeft()
        
        //Assert //Checks
        XCTAssertFalse(roverDataManager.direction == "W")
    }

    func testPositive_spinRight() {
        //Arrange //Data
        roverDataManager = RoverDataManager(location: "1 2 N")
        
        //Act //Calls
        roverDataManager.spinRight()
        
        //Assert //Checks
        XCTAssertEqual("S", roverDataManager.direction)
    }
    
    func testNegative_spinRight() {
        //Arrange //Data
        roverDataManager = RoverDataManager(location: "1 2 N")
        
        //Act //Calls
        roverDataManager.spinRight()
        
        //Assert //Checks
        XCTAssertFalse(roverDataManager.direction == "W")
    }
    
    func testPositive_stepForward() {
        //Arrange //Data
        roverDataManager = RoverDataManager(location: "1 2 N")
        
        //Act //Calls
        roverDataManager.stepForward()
        
        //Assert //Checks
        XCTAssertEqual(3, roverDataManager.y)
    }
    
    func testNegative_stepForward() {
        //Arrange //Data
        roverDataManager = RoverDataManager(location: "1 2 N")
        
        //Act //Calls
        roverDataManager.spinRight()
        
        //Assert //Checks
        XCTAssertFalse(roverDataManager.y == 0)
    }
    
    func testPositive_travelToLocation() {
        //Arrange //Data
        roverDataManager = RoverDataManager(location: "1 2 N")
        
        //Act //Calls
        roverDataManager.travelToLocation(command: "LMLMLMLMM")
        
        //Assert //Checks
        XCTAssertEqual("1 3 N", "\(roverDataManager.x) \(roverDataManager.y) \(roverDataManager.direction)")
    }
    
    func testNegative_travelToLocation() {
        //Arrange //Data
        roverDataManager = RoverDataManager(location: "1 2 N")
        
        //Act //Calls
        roverDataManager.travelToLocation(command: "LMLMLMLMM")
        
        //Assert //Checks
        XCTAssertFalse("\(roverDataManager.x) \(roverDataManager.y) \(roverDataManager.direction)" == "1 4 N")
    }
    
    func testPositive_checkIfSumOfCoordinateIsPrime() {
        //Arrange //Data
        roverDataManager = RoverDataManager(location: "1 2 N")
        
        //Act //Calls
        let result = roverDataManager.checkIfSumOfCoordinateIsPrime(x: roverDataManager.x, y: roverDataManager.y)
        
        //Assert //Checks
        XCTAssertTrue(result)
    }
    
    func testNegative_checkIfSumOfCoordinateIsPrime() {
        //Arrange //Data
        roverDataManager = RoverDataManager(location: "1 5 N")
        
        //Act //Calls
        let result = roverDataManager.checkIfSumOfCoordinateIsPrime(x: roverDataManager.x, y: roverDataManager.y)
        
        //Assert //Checks
        XCTAssertFalse(result)
    }
}
